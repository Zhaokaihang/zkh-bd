#!/bin/bash

# 初始化部分颜色 31:红色 32:绿色 34:蓝色
red="\033[31m"
green="\033[32m"
blue="\033[34m"
end="\033[0m"

echo $red 1：文件管理 $end
echo '''ls命令 – 显示目录中文件及其属性信息
cp命令 – 复制文件或目录
mv命令 – 移动或改名文件
mkdir命令 – 创建目录文件
pwd命令 – 显示当前工作目录的路径
tar命令 – 压缩和解压缩文件
cd命令 – 切换目录
chmod命令 – 改变文件或目录权限 '''

echo $red 2：文档编辑 $end
echo '''cat命令 – 在终端设备上显示文件内容 
命令 – 输出字符串或提取后的变量值 
rm命令 – 删除文件或目录 
grep命令 – 强大的文本搜索工具 
tail命令 – 查看文件尾部内容 
rmdir命令 – 删除空目录文件 
sed命令 – 批量编辑文本文件 
vi命令 – 文本编辑器'''

echo $red 3：系统管理 $end
echo '''find命令 – 根据路径和条件搜索指定文件 
rpm命令 – RPM软件包管理器 
ps命令 – 显示进程状态 
startx命令 – 初始化X-window系统 
uname命令 – 显示系统内核信息 
resize2fs命令 – 同步文件系统容量到内核 
kill命令 – 杀死进程 
useradd命令 – 创建并设置用户信息 '''

echo $red 4：磁盘管理 $end

echo '''df命令 – 显示磁盘空间使用量情况 
fdisk命令 – 管理磁盘分区 
lsblk命令 – 查看系统的磁盘使用情况 
vgextend命令 – 扩展卷组设备 
hdparm命令 – 显示与设定硬盘参数 
mkfs.ext4 命令 – 对磁盘设备进行EXT4格式化 
pvcreate命令 – 创建物理卷设备 
lvcreate命令 – 创建逻辑卷设备'''


echo $red 5：文件传输 $end
echo '''tftp命令 – 上传及下载文件 
curl命令 – 文件传输工具 
fsck命令 – 检查与修复文件系统 
lprm命令 – 移除打印队列中的任务 
ftpwho命令 – 显示FTP会话信息 
rsync命令 – 远程数据同步工具 
ftp命令 – 文件传输协议客户端 
lftp命令 – 优秀的命令行FTP客户端 '''

echo $red 6：网络通讯 $end
echo '''ssh命令 – 安全的远程连接服务 
netstat命令 – 显示网络状态 
dhclient命令 – 动态获取或释放IP地址 
ifconfig命令 – 显示或设置网络设备参数信息
ping命令 – 测试主机间网络连通性 
sshd命令 – openssh服务器守护进程 
iptables命令 – 防火墙策略管理工具
smbpasswd命令 – 修改用户的SMB密码''' 

echo $red 7：设备管理 $end
echo '''mount命令 – 将文件系统挂载到目录 
lspci命令 – 显示当前设备PCI总线设备信息 
sensors命令 – 检测服务器硬件信息
setleds命令 – 设置键盘的LED灯光状态 
rfkill命令 – 管理蓝牙和WIFI设备
setpci命令 – 配置PCI硬件设备参数 
hciconfig命令 – 配置蓝牙设备 
lsusb命令 – 显示USB设备列表 '''

echo $red 8：备份压缩 $end
echo '''zip命令 – 压缩文件 
unzip命令 – 解压缩zip格式文件 
gzip命令 – 压缩和解压文件 
zipinfo命令 – 查看压缩文件信息 
gunzip命令 – 解压提取文件内容 
unarj命令 – 解压.arj文件 
zipsplit命令 – 分割压缩包 
dump命令 – 备份文件系统 '''

echo $red 9：其他命令 $end
echo '''hash命令 – 管理命令运行时查询的哈希表 
wget命令 – 下载网络文件
wait命令 – 等待指令执行完毕 
history命令 – 显示与管理历史命令记录 
bc命令 – 数字计算器 
rmmod命令 – 移除内核模块 
pigz命令 – 多线程的解压缩文件 
xargs命令 – 给其他命令传参数的过滤器'''