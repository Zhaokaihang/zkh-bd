workdir=~/.bd-zkh/bin
mkdir -p $workdir
if [[ $PATH =~ $workdir ]]; then
    # 已经设置环境变量
    echo '环境变量已配置'
else
    echo '开始设置环境变量'

    echo 'export PATH=$PATH:'$workdir >>~/.bashrc
    source ~/.bashrc

    #bash_profile 里面会引用bashrc的配置
    echo 'export PATH=$PATH:'$workdir >>~/.bashrc
    source ~/.bash_profile

fi
export PATH=$PATH:$workdir

cat >$workdir/bd-zkh <<EOF
workdir=~/.bd-zkh/bin

curl -o $workdir/lastest-asd-bz https://gitea.com/Zhaokaihang/zkh-bd/raw/branch/main/lastest-asd-bz 2> /dev/null

chmod +x $workdir/lastest-asd-bz
lastest-asd-bz \$@
EOF
cp $workdir/bd-zkh $workdir/bz
chmod +x $workdir/*
echo "安装完毕"
